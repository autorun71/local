/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/main.js":
/*!******************************!*\
  !*** ./resources/js/main.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  $('input[name=token_id][value=""]').val(Math.random(36).toString(36).substring(2, 15) + Math.random(36).toString(36).substring(2, 15) + Math.random(36).toString(36).substring(2, 15));
  $('#defaultOrderButton').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/terminal',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify([{
        "order_type": "default"
      }]),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        $('#order-num').html("<h2>" + response.client_num + "</h2>");
        $('#order-length').text(response.order_length);
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#onlineOrderButton').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/terminal',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify([{
        "order_type": "online"
      }]),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        $('#online-order-num').html("<h2>" + response.client_num + "</h2>");
        $('#online-order-length').text(response.order_length);
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#nextClientCashBox1Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/next',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "cashbox_num": 1,
        "order_type": "default"
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №1');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        }
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#repeatClientCashBox1Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/repeat',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "cashbox_num": 1,
        "order_type": "default"
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №1');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        }
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#stopClientCashBox1Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/stop',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "cashbox_num": 1,
        "order_type": "default"
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №1');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        } //$('#message-cashbox-1').html("<h2>" + response.message + "</h2>");

      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#nextClientCashBox2Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/next',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "cashbox_num": 2,
        "order_type": "online"
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №2');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        }
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#repeatClientCashBox2Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/repeat',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "cashbox_num": 2,
        "order_type": "online"
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №2');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        }
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#stopClientCashBox2Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/stop',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "cashbox_num": 2,
        "order_type": "online"
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №2');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        } //$('#message-cashbox-1').html("<h2>" + response.message + "</h2>");

      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#nextClientDefaultCashBox3Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/next',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "message": {
          "token_id": "sbmngs9rbdpp4dphw2alcpncscrxrod"
        }
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №3');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        }
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#nextClientOnlineCashBox3Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/next',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "message": {
          "token_id": "sbmngs9rbdpp4dphw2alcpncscrxrod"
        }
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №3');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        }
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#repeatClientCashBox3Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/repeat',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "message": {
          "token_id": "sbmngs9rbdpp4dphw2alcpncscrxrod"
        }
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №3');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        }
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#stopClientCashBox3Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/stop',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "message": {
          "token_id": "sbmngs9rbdpp4dphw2alcpncscrxrod"
        }
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №3');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        } //$('#message-cashbox-1').html("<h2>" + response.message + "</h2>");

      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#nextClientCashBox4Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/next',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "message": {
          "token_id": "8abnzm6y60h199goimxpg4rv3dfvzmua"
        }
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №4');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        }
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#repeatClientCashBox4Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/repeat',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "message": {
          "token_id": "8abnzm6y60h199goimxpg4rv3dfvzmua"
        }
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №4');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        }
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#stopClientCashBox4Button').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      url: '/api/cashbox/stop',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "message": {
          "token_id": "8abnzm6y60h199goimxpg4rv3dfvzmua"
        }
      }),
      dataType: 'json',
      success: function success(response, textStatus, jqXHR) {
        console.log(response); //alert(response);

        if (typeof response.message !== "undefined") {
          $('#modalLabel').text('Сообщение оператору');
          $('#message-cashbox').html("<h2>" + response.message + "</h2>");
        } else {
          $('#modalLabel').text('Табло кассы №4');
          $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
        } //$('#message-cashbox-1').html("<h2>" + response.message + "</h2>");

      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log('error(s):' + textStatus, errorThrown);
      }
    });
  });
  $('#terminalDefaultOrderButton').click(function () {
    $('#terminal-form').append("<input type='hidden' name='defaultOrderButton' value='true' />");
    event.preventDefault(); //$("body").addClass('opacity');

    setTimeout(function () {
      $('#terminal-form').submit();
    }, 100); //$("<p>Delay...</p>").appendTo("body");
  });
  $('#terminalOnlineOrderButton').click(function () {
    $('#terminal-form').append("<input type='hidden' name='onlineOrderButton' value='true' />");
    event.preventDefault();
    setTimeout(function () {
      $('#terminal-form').submit();
    }, 100);
  });
});

/***/ }),

/***/ 2:
/*!************************************!*\
  !*** multi ./resources/js/main.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/www-01/resources/js/main.js */"./resources/js/main.js");


/***/ })

/******/ });