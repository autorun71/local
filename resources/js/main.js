$(document).ready(function () {

    $('input[name=token_id][value=""]').val(Math.random(36).toString(36).substring(2, 15) + Math.random(36).toString(36).substring(2, 15) + Math.random(36).toString(36).substring(2, 15));

    $('#defaultOrderButton').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/terminal',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify([
                {
                    "order_type": "default"
                }
            ]),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                $('#order-num').html("<h2>" + response.client_num + "</h2>");
                $('#order-length').text(response.order_length);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#onlineOrderButton').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/terminal',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify([
                {
                    "order_type": "online"
                }
            ]),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                $('#online-order-num').html("<h2>" + response.client_num + "</h2>");
                $('#online-order-length').text(response.order_length);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#nextClientCashBox1Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/next',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "cashbox_num": 1,
                "order_type": "default"
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №1');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#repeatClientCashBox1Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/repeat',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "cashbox_num": 1,
                "order_type": "default"
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №1');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#stopClientCashBox1Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/stop',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "cashbox_num": 1,
                "order_type": "default"
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №1');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
                //$('#message-cashbox-1').html("<h2>" + response.message + "</h2>");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#nextClientCashBox2Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/next',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "cashbox_num": 2,
                "order_type": "online"
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №2');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#repeatClientCashBox2Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/repeat',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "cashbox_num": 2,
                "order_type": "online"
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №2');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#stopClientCashBox2Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/stop',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "cashbox_num": 2,
                "order_type": "online"
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №2');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
                //$('#message-cashbox-1').html("<h2>" + response.message + "</h2>");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#nextClientDefaultCashBox3Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/next',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "message": {
                    "token_id": "sbmngs9rbdpp4dphw2alcpncscrxrod"
                }
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №3');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#nextClientOnlineCashBox3Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/next',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "message": {
                    "token_id": "sbmngs9rbdpp4dphw2alcpncscrxrod"
                }
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №3');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#repeatClientCashBox3Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/repeat',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "message": {
                    "token_id": "sbmngs9rbdpp4dphw2alcpncscrxrod"
                }
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №3');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#stopClientCashBox3Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/stop',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "message": {
                    "token_id": "sbmngs9rbdpp4dphw2alcpncscrxrod"
                }
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №3');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
                //$('#message-cashbox-1').html("<h2>" + response.message + "</h2>");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#nextClientCashBox4Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/next',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "message": {
                    "token_id": "8abnzm6y60h199goimxpg4rv3dfvzmua",
                }
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №4');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#repeatClientCashBox4Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/repeat',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "message": {
                    "token_id": "8abnzm6y60h199goimxpg4rv3dfvzmua"
                }
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №4');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#stopClientCashBox4Button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/cashbox/stop',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "message": {
                    "token_id": "8abnzm6y60h199goimxpg4rv3dfvzmua"
                }
            }),
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                //alert(response);
                if (typeof response.message !== "undefined") {
                    $('#modalLabel').text('Сообщение оператору');
                    $('#message-cashbox').html("<h2>" + response.message + "</h2>");
                } else {
                    $('#modalLabel').text('Табло кассы №4');
                    $('#message-cashbox').html("<h2>" + response.client_num + "</h2>");
                }
                //$('#message-cashbox-1').html("<h2>" + response.message + "</h2>");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error(s):' + textStatus, errorThrown);
            }
        });
    });

    $('#terminalDefaultOrderButton').click(function () {

        $('#terminal-form').append("<input type='hidden' name='defaultOrderButton' value='true' />");

        event.preventDefault();

        //$("body").addClass('opacity');

        setTimeout(function () {
            $('#terminal-form').submit();
        }, 100);

        //$("<p>Delay...</p>").appendTo("body");
    });

    $('#terminalOnlineOrderButton').click(function () {

        $('#terminal-form').append("<input type='hidden' name='onlineOrderButton' value='true' />");

        event.preventDefault();

        setTimeout(function () {
            $('#terminal-form').submit();
        }, 100);
    });
});
