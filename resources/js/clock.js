var serverTime = new Date();

window.updateTime = function updateTime() {

    serverTime = new Date(serverTime.getTime() + 1000);

    //Get time
    var hours = serverTime.getHours();
    var minutes = serverTime.getMinutes();
    var seconds = serverTime.getSeconds();

    $('.time-footer').html(hours + "<span class='separator'>:</span>" + (minutes < 10 ? "0" : "") + minutes);

    var day = serverTime.getDate();
    var month = serverTime.getMonth() + 1;
    var year = serverTime.getFullYear();

    $('.date-footer').html((day < 10 ? "0" : "") + day + "." + (month < 10 ? "0" : "") + month + "." + year);

    //Remove sleep message and opacity
    $("#main-content").removeClass('opacity');
    $("#sleep-message").hide();
}

$(document).ready(function () {

    setInterval('updateTime()', 1000);
});
