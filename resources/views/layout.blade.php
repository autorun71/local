<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!--<link rel="stylesheet" href="css/bootstrap.min.css">-->
    <link rel="stylesheet" href="css/app.css">

    <title>Электронная очередь</title>
</head>
<body style="height: 100vh">

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">Электронная очередь
        <?php
        $env = config('app.env');
        if ($env != "prod")
            echo "($env)";
        ?>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Терминал
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/">Прототип</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/device/terminal/Mc367qt3oeQiRU7IlHOEUE5v2Zc4gzZRis3STuQACkI=">Устройство</a>
                    <!--<a class="dropdown-item" href="/model">Модель</a>-->
                </div>
            </li>
            <!--<li class="nav-item">
                <a class="nav-link" href="/">Терминал</a>
            </li>-->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="/board" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Табло
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/board">Прототип</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/device/board">Устройство</a>
                    <!--<a class="dropdown-item" href="/model">Модель</a>-->
                </div>
            </li>
            <!--<li class="nav-item">
                <a class="nav-link" href="/board">Табло</a>
            </li>-->
            <li class="nav-item">
                <a class="nav-link" href="/cashbox">Кассы</a>
            </li>
        </ul>
        <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="/admin">Администрирование</a>
            </li>
        </ul>

        <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Поиск" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Поиск</button>
        </form>-->
    </div>
</nav>

<!-- Show error -->
@if ($errors->any())
    <div class="alert alert-danger text-center">
        <!--<ul>-->
    @foreach ($errors->all() as $error)
        <!--<li>-->{{ $error }}<!--</li>-->
    @endforeach
    <!--</ul>-->
    </div>
@endif

<!-- Show messages -->
@if(session()->has('message'))
    <div class="alert alert-success text-center">
        {{ session()->get('message') }}
    </div>
@endif

<main role="main" class="container h-100">

    <div class="row h-100 justify-content-center align-items-center">
        @yield('index')
        @yield('board')
        @yield('cashboxes')
        @yield('admin.admin')
    </div>
</main>

<!--<footer class="footer">
    <div class="container">
        <span class="text-muted">Place sticky footer content here.</span>
    </div>
</footer>-->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!--<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>-->
<!--<script src="js/popper.min.js" type="text/javascript"></script>-->
<!--<script src="js/bootstrap.min.js" type="text/javascript"></script>-->
<!--<script src="js/main.js" type="text/javascript"></script>-->
<script src="js/app.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>
</body>
</html>
