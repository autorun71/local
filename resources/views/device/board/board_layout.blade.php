<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/app.css">

    <title>Электронная очередь</title>
</head>
<body class="vh-100">
@yield('board')
@yield('board_auth')
<!--<script src="/js/jquery-3.3.1.min.js" type="text/javascript"></script>-->
<!--<script src="/js/jquery.cookie.js" type="text/javascript"></script>-->
<!--<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>-->
<!--<script src="/js/js.cookie.min.js"></script>-->
<!--<script src="/js/jquery.dataTables.min.js" type="text/javascript"></script>-->
<!--<script src="/js/datatables.js" type="text/javascript"></script>-->
<!--<script src="/js/clock.js" type="text/javascript"></script>-->
<!--<script src="/js/howler.js" type="text/javascript"></script>-->
<!--<script src="/js/test.js" type="text/javascript"></script>-->
<script src="/js/app.js" type="text/javascript"></script>
<script src="/js/main.js" type="text/javascript"></script>
<script src="/js/clock.js" type="text/javascript"></script>
<script src="/js/board.js" type="text/javascript"></script>
</body>
</html>
