@extends('device.board.board_layout')
@section('board')

    <div hidden class="container-fluid font-weight-bold h-100 position-absolute"
         style="font-family: Arial; color: #F4154E; z-index: 100;" id="call-message">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="blink display-1 align-middle">
                <span id="client-num">А001</span>
                <span>ОКНО №</span>
                <span id="cashbox-num">2</span>
            </div>
        </div>
    </div>

    <div class="container-fluid h-100 w-100 mx-auto" id="main-content">
        <table class="w-100">
            <tr>
                <td class="w-50">
                    <div class="row justify-content-center">
                        <div><img src="/storage/{{ setting('site.pharmacy_logo') }}" class="logo" alt=""></div>
                        <div class="pharmacy-header pt-2">{{ setting('site.pharmacy_view_name') }}</div>
                    </div>
                </td>
                <td class="w-50">
                    <div class="row justify-content-center">
                        <div class="pharmacy-header">{{ setting('site.board_header') }}</div>
                    </div>
                </td>
            </tr>
            <tr style="max-height: 500px;">
                <td class="align-top w-50">
                    <div class="h-50">
                        <img src="/storage/{{ setting('site.board_banner') }}" class="img-fluid"/>
                    </div>
                </td>
                <td class="align-top w-50">
                    <table id="orderBoardTable" class="table table-striped">
                        <thead>
                        <tr class="board-header">
                            <th style="width: 40%">КЛИЕНТ</th>
                            <th style="width: 20%"></th>
                            <th style="width: 40%">ОКНО</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="order-row-0"><td id="client-num"></td><td id="arrow"></td><td id="cashbox-num"></td></tr>
                        <tr id="order-row-1"><td id="client-num"></td><td id="arrow"></td><td id="cashbox-num"></td></tr>
                        <tr id="order-row-2"><td id="client-num"></td><td id="arrow"></td><td id="cashbox-num"></td></tr>
                        <tr id="order-row-3"><td id="client-num"></td><td id="arrow"></td><td id="cashbox-num"></td></tr>
                        <tr id="order-row-4"><td id="client-num"></td><td id="arrow"></td><td id="cashbox-num"></td></tr>
                        <tr id="order-row-5"><td id="client-num"></td><td id="arrow"></td><td id="cashbox-num"></td></tr>
                        <tr id="order-row-6"><td id="client-num"></td><td id="arrow"></td><td id="cashbox-num"></td></tr>
                        <tr id="order-row-7"><td id="client-num"></td><td id="arrow"></td><td id="cashbox-num"></td></tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        <hr>
        <div class="col-12 justify-content-end d-flex">
            <span class="time-footer"></span>
            <span class="date-footer"></span>
        </div>
    </div>
@stop
