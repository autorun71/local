@extends('device.terminal.terminal_layout')
@section('terminal_index')

    <div class="col-12 text-center position-absolute" id="sleep-message" style="top: 45%; z-index: 1; right: 0px; left: 0px;">
        <h1>Подождите, идёт печать талона</h1>
    </div>

    <div class="container opacity" id="main-content">
        <div class="container pt-2 pt-md-3">

            <div class="row justify-content-center">

                <div class="row justify-content-center">
                    <div><img src="{{ '/storage/' . setting('site.pharmacy_logo') }}" class="logo"/></div>
                    <div class="pharmacy-header pt-2">{{ setting('site.pharmacy_view_name') }}</div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <hr/>
                </div>
            </div>

        </div>

        <div class="container pt-1 pt-md-2 pb-3 pb-md-4">
            <div class="col-md-12 d-flex justify-content-center">
                <span class="please-header text-center text-md-left">{{ setting('site.terminal_hello') }}</span>
            </div>
        </div>

        <div class="container">
            <div class="form-group">
                <form action="{!! action('TerminalController@returnUrl') !!}" method="POST" id="terminal-form">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <div class="pr-0 pr-lg-5">
                                <button type="submit" class="btn btn-danger btn-lg btn-block"
                                        id="terminalDefaultOrderButton" name="defaultOrderButton" value="default">
                                    ОСНОВНАЯ
                                    ОЧЕРЕДЬ
                                </button>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="pl-0 pl-lg-5">
                                <button type="submit" class="btn btn-danger btn-lg btn-block"
                                        id="terminalOnlineOrderButton"
                                        name="onlineOrderButton" value="online">ИНТЕРНЕТ-ЗАКАЗ
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row h-100">
                <div class="col-md-12">
                    <img src="/storage/{{ setting('site.terminal_banner') }}" class="img-fluid" style="max-height: 300px; width: auto;"/>
                </div>
            </div>
        </div>

        <div class="container justify-content-center">
            <div class="row">
                <div class="col-md-12 d-flex justify-content-center">
                    <span class="time-footer"></span>
                    <span class="date-footer"></span>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-12">
                    <hr>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-12 center-block text-center pb-3 pb-md-4">
                    <span class="info-footer">{{ setting('site.telephone_header') . " " . setting('site.pharmacy_tel') }}</span>
                </div>
            </div>
        </div>
    </div>
@stop
