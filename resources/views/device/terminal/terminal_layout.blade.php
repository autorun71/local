<!doctype html>
<html lang="ru" class="h-100">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/app.css" />

    <title>Электронная очередь</title>
</head>
<!--<body class="h-100 disable-copy" oncontextmenu="return false;" onmousedown='return false;' onselectstart="return false">-->
<body>
<div class="container h-100" id="app">
    @yield('terminal_index')
</div>
<script src="/js/app.js" type="text/javascript"></script>
<script src="/js/clock.js" type="text/javascript"></script>
<script src="/js/main.js" type="text/javascript"></script>
</body>
</html>
