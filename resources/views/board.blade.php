@extends('layout')
@section('board')
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <table class="table">
                    <tr>
                        <th class="h4">Основная очередь</th>
                    </tr>
                    @foreach($defaultOrderArray as $orderNum)
                        <tr>
                            <td class="h2">{{ $orderNum->client_num }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-sm">
                <table class="table">
                    <tr>
                        <th class="h4">Интернет-заказ</th>
                    </tr>
                    @foreach($onlineOrderArray as $orderNum)
                        <tr>
                            <td class="h2">{{ $orderNum->client_num }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-sm">
                <table class="table">
                    @foreach($activeClients as $activeClient)
                        <tr>
                            <td class="text-danger h2">{{ $activeClient->client_num }}</td>
                            <td class="text-danger h2">-</td>
                            </td>
                            <td class="text-danger h2">Касса №{{ $activeClient->cashbox_num }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@stop
