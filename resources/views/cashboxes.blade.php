@extends('layout')
@section('cashboxes')

    <div class="container">
        <div class="row">

            <!-- Cashbox 1 -->
            <div class="col-sm-6">
                <div class="container">
                    <div class="row">
                        <div class="col-sm h3">Касса №1
                            <div class="h6">Основная очередь</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-success btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="nextClientCashBox1Button">
                                Следующий клиент
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-warning btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="repeatClientCashBox1Button">
                                Вызвать следующего клиента повторно
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-danger btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="stopClientCashBox1Button">
                                Завершить обслуживание клиента
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Cashbox 2 -->
            <div class="col-sm-6">
                <div class="container">
                    <div class="row">
                        <div class="col-sm h3">Касса №2
                            <div class="h6">Интернет-заказ</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-success btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="nextClientCashBox2Button">
                                Следующий клиент
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-warning btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="repeatClientCashBox2Button">
                                Вызвать следующего клиента повторно
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-danger btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="stopClientCashBox2Button">
                                Завершить обслуживание клиента
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">

            <!-- Cashbox 3 -->
            <div class="col-sm">
                <div class="container">
                    <div class="row">
                        <div class="col-sm h3">Касса №3
                            <div class="h6">С возможностью выбора очереди</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-outline-success btn-lg-cashbox"
                                        data-toggle="modal"
                                        data-target="#modalCashBox" id="nextClientDefaultCashBox3Button">
                                    Следующий клиент - Основная очередь
                                </button>
                                <button type="button" class="btn btn-outline-success btn-lg-cashbox"
                                        data-toggle="modal"
                                        data-target="#modalCashBox" id="nextClientOnlineCashBox3Button">
                                    Следующий клиент - Интернет-заказ
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-warning btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="repeatClientCashBox3Button">
                                Вызвать следующего клиента повторно
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-danger btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="stopClientCashBox3Button">
                                Завершить обслуживание клиента
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Cashbox 4 -->
            <div class="col-sm-6">
                <div class="container">
                    <div class="row">
                        <div class="col-sm h3">Касса №4
                            <div class="h6">Автоматический выбор из двух очередей по времени ожидания</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-success btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="nextClientCashBox4Button">
                                Следующий клиент
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-warning btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="repeatClientCashBox4Button">
                                Вызвать следующего клиента повторно
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-danger btn-lg-cashbox btn-block"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="stopClientCashBox4Button">
                                Завершить обслуживание клиента
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
    <div class="modal fade" id="modalCashBox" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Табло кассы</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="message-cashbox"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop
