@extends('layout')
@section('index')

    <div class="container justify-content-center">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <!-- Default order -->
                <button type="button" class="btn btn-danger btn-lg btn-block" data-toggle="modal"
                        data-target="#defaultModal"
                        id="defaultOrderButton">
                    Основная очередь
                </button>

                <!-- Modal -->
                <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Талон</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div id="order-num"></div>
                                <div>Перед вами <span id="order-length"></span> человек(а)</div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>

        <div class="row mt-4">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <!-- Online order -->
                <button type="button" class="btn btn-danger btn-lg btn-block" data-toggle="modal"
                        data-target="#onlineModal" id="onlineOrderButton">
                    Интернет-заказ
                </button>

                <!-- Modal -->
                <div class="modal fade" id="onlineModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Талон</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div id="online-order-num"></div>
                                <div>Перед вами <span id="online-order-length"></span> человек(а)</div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
@stop
