FROM php:7.2-fpm


RUN apt-get update
RUN docker-php-ext-install pdo pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
WORKDIR /var/www/laravel-docker
CMD ["php-fpm"]
