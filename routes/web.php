<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//Swagger
Route::get('/swagger', function(){
    return file_get_contents(storage_path() . "/swagger/$file_name");
});

//Prototype
Route::get('/', 'PrototypeController@index');
Route::get('/board', 'BoardController@index');
Route::get('/cashbox', 'CashboxController@index');

//Devices
//Terminal
Route::get('/device/terminal', 'TerminalController@device');
Route::get('/device/terminal/success/{order_type}', 'TerminalController@device');
Route::get('/device/terminal/error/{token_id}', 'TerminalController@device');
Route::post('/device/terminal/click', 'TerminalController@returnUrl');

//Board
Route::get('/device/board', 'BoardController@deviceIndex');

//Swagger
Route::get('/swagger/{file_name}', 'SwaggerController@index');

//Ajax
Route::get('/ajax/time', 'AjaxController@time');

//Clear cache
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('route:cache');
    return "Cache is cleared";
});
