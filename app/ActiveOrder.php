<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveOrder extends Model
{
    public function cashbox()
    {
        return $this->hasOne('App\Cashbox', 'id', 'cashbox_id');
    }
}
