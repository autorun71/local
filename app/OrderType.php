<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderType extends Model
{
    public function pharmacies() {

        return $this->belongsToMany('App\Pharmacy', 'pharmacy_order');
    }

    public function cashboxes() {

        return $this->belongsToMany('App\Cashbox', 'cashbox_order');
    }
}
