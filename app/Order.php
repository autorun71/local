<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    public $timestamps = false;

    public function type() {
        return $this->hasOne('App\OrderType', 'id', 'order_type_id');
    }
}
