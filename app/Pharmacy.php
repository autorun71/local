<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pharmacy extends Model
{
    protected $table = 'pharmacies';

    public function orderTypes()
    {
        return $this->belongsToMany('App\OrderType', 'pharmacy_order');
    }

    public function pharmacyNetwork()
    {
        return $this->hasOne('App\PharmacyNetwork', 'id', 'pharmacy_network_id');
    }
}
