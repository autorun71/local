<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActiveOrder;
use App\Order;
use App\LogOrder;

class CashboxController extends Controller
{

    private $cashboxNum;
    private $orderType;
    private $clientId;

    //Get next client from order
    public function getNextClient(Request $request)
    {

        $requestArray = $request->json()->all();

        $this->cashboxNum = $requestArray['cashbox_num'];
        $this->orderType = $requestArray['order_type'];

        $this->stopCurrentClient($request);
        return $this->setClientActive();
    }

    //Get next client from order
    public function getNextClientById(Request $request)
    {

        $requestArray = $request->json()->all();

        $this->cashboxNum = $requestArray['cashbox_num'];
        $this->clientId = $requestArray['client_id'];

        $this->stopCurrentClient($request);
        return $this->setClientActive();
    }

    public function returnClient(Request $request) {

        $requestArray = $request->json()->all();

        $this->cashboxNum = $requestArray['cashbox_num'];

        $client = ActiveOrder::where('cashbox_num', $this->cashboxNum)->first();

        $order = new Order();
        $order->order_type_id = $client->order_type_id;
        $order->client_num = $client->client_num;
        $order->start_time = $client->start_time;

        if($order->save()) {
            if(ActiveOrder::destroy($client->id)) {
                return json_encode([
                    "message" => 'Клиент успешно возвращён в очередь'
                ], JSON_UNESCAPED_UNICODE);
            }
        }
    }

    //Repeat call next client from active order
    public function repeatNextClient(Request $request)
    {

        $requestArray = $request->json()->all();

        $this->cashboxNum = $requestArray['cashbox_num'];

        $activeClient = $this->getActiveClient();

        if (count((array)$activeClient) == 0) {
            return $this->setClientActive();
        } else {
            //Sound call client
            $activeClient->sound_call = true;
            $activeClient->save();
        }

        return json_encode([
            "client_num" => $activeClient->client_num
        ], JSON_UNESCAPED_UNICODE);
    }

    //Finish service current active client
    public function stopCurrentClient(Request $request)
    {

        $requestArray = $request->json()->all();

        $this->cashboxNum = $requestArray['cashbox_num'];

        if ($this->getActiveClientCount() == 0) {
            return json_encode([
                "message" => 'У вас нет активных клиентов'
            ], JSON_UNESCAPED_UNICODE);
        }

        $activeClient = $this->getActiveClient();

        $logOrder = new LogOrder();

        $logOrder->order_type_id = $activeClient->order_type_id;
        $logOrder->client_num = $activeClient->client_num;
        $logOrder->cashbox_num = $activeClient->cashbox_num;
        $logOrder->start_time = $activeClient->start_time;
        $logOrder->active_time = $activeClient->active_time;
        $logOrder->end_time = $activeClient->end_time = \Carbon\Carbon::now();

        if ($logOrder->save()) {

            //Delete client from orders table
            ActiveOrder::destroy($activeClient->id);
        }

        return json_encode([
            "message" => "Обслуживание клиента завершено"
        ], JSON_UNESCAPED_UNICODE);
    }

    //Get orders list
    public function getList(Request $request) {

        $orderList = Order::orderBy('start_time');
        if (!empty($request->order_type)) {
            $orderList = $orderList->join('order_types', 'order_types.id', '=', 'orders.order_type_id')->where('order_types.alias', $request->order_type);
        }
        $orderList = $orderList->select(
            'orders.id',
            'client_num',
            'order_type_id'
        )->get();

        $orderListArray = [];

        //Recompile array
        foreach($orderList as $list) {
            $orderListArray[] = [
                'id' => $list->id,
                'client_num' => $list->client_num,
                'order_type' => $list->type->name,
            ];
        }

        return json_encode($orderListArray, JSON_UNESCAPED_UNICODE);
    }

    //Move client from orders model to active_orders model
    private function setClientActive()
    {
        if ($this->getActiveClientCount() != 0) {
            return json_encode([
                "message" => 'Завершите обслуживание текущего клиента'
            ], JSON_UNESCAPED_UNICODE);
        }

        $nextClient = Order::orderBy('start_time');

        //If set order_type
        if(isset($this->orderType)) {
            $nextClient->join('order_types', 'order_types.id', '=', 'orders.order_type_id')->where('order_types.alias', $this->orderType);
        }

        //If set client_id
        if(isset($this->clientId)) {
            $nextClient->where('orders.id', $this->clientId);
        }

        $nextClient = $nextClient->select(
            'orders.id AS id',
            'order_type_id',
            'client_num',
            'start_time'
        )->first();

        //Check for active clients
        if (empty($nextClient->id)) {
            return json_encode([
                "message" => 'Нет клиентов в очереди'
            ], JSON_UNESCAPED_UNICODE);
        }

        //Insert new client to active_orders table
        $activeOrder = new ActiveOrder();

        $activeOrder->order_type_id = $nextClient->order_type_id;
        $activeOrder->client_num = $nextClient->client_num;
        $activeOrder->sound_call = true;
        $activeOrder->cashbox_num = $this->cashboxNum;
        $activeOrder->start_time = $nextClient->start_time;
        $activeOrder->active_time = \Carbon\Carbon::now();

        if ($activeOrder->save()) {

            //Delete client from orders table
            Order::destroy($nextClient->id);
        }

        return json_encode([
            "client_id" => $nextClient->id,
            "client_num" => $nextClient->client_num
        ], JSON_UNESCAPED_UNICODE, JSON_NUMERIC_CHECK);
    }

    //Get number of active client for cashbox
    private function getActiveClientCount() {
        return ActiveOrder::where('cashbox_num', '=', $this->cashboxNum)->count();
    }

    //Get active client for cashbox
    private function getActiveClient() {
        return ActiveOrder::where('cashbox_num', '=', $this->cashboxNum)->first();
    }
}
