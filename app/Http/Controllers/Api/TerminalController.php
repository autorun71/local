<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderType;

class TerminalController extends Controller
{
    public function getOrderNum(Request $request)
    {
        $requestArray = $request->json()->all();

        //dd($requestArray);
        $orderAlias = $requestArray['order_type'];

        $orderInfo = json_decode($this->getOrderInfo($orderAlias), true);

        Order::insert(
            [
                'order_type_id' => $orderInfo['order_type_id'],
                'client_num' => $orderInfo['client_num'],
                'start_time' => \Carbon\Carbon::now()
            ]
        );

        return json_encode(['client_num' => $orderInfo['client_num'], 'order_length' => $orderInfo['order_length']], JSON_UNESCAPED_UNICODE);
    }

    private function getOrderInfo($orderAlias)
    {

        $orderType = OrderType::where('alias', $orderAlias)->first();
        $orderValue = $orderType->counter + 1;

        //Format client num
        $clientNum = $orderType->prefix . sprintf('%03d', $orderValue);

        //Zeroing counter if more than 99
        if ($orderValue >= 20) {
            $orderValue = 0;
        }

        //Update counter
        OrderType::where('id', $orderType->id)->update(['counter' => $orderValue]);

        //Get order length
        $orderLength = Order::where('order_type_id', $orderType->id)->count();

        return json_encode([
            'client_num' => $clientNum,
            'order_length' => $orderLength,
            'order_type_id' => $orderType->id
        ], JSON_UNESCAPED_UNICODE);
    }
}
