<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActiveOrder;

class BoardController extends Controller
{
    public function getActiveListApi(Request $request)
    {
        $order['data'] = ActiveOrder::orderBy('updated_at', 'DESC')->get();
        return json_encode($order, JSON_UNESCAPED_UNICODE);
    }

    public function getSoundClient(Request $request)
    {
        $order = ActiveOrder::where('sound_call', '=', true)->orderBy('active_time')->first();
        return json_encode($order, JSON_UNESCAPED_UNICODE);
    }

    public function disableSoundClient($id)
    {
        $activeOrder = ActiveOrder::find($id);
        $activeOrder->sound_call = false;
        $activeOrder->save();
    }
}
