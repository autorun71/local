<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\PharmacyTrait;
use App\Http\Traits\RequestTrait;
use App\Order;
use App\OrderType;
use App\Pharmacy;
use App\ActiveOrder;
use DB;
use Log;
use Cookie;

class BoardController extends Controller
{

    public function index()
    {
        return $this->getOrderList();
    }

    public function deviceIndex()
    {
        return view('device.board.board');
    }

    private function getOrderList()
    {

        //Default
        $orderAlias = 'default';
        $defaultOrderArray = Order::join('order_types', 'order_type_id', '=', 'order_types.id')->where('alias', $orderAlias)->get('client_num');

        //Online
        $orderAlias = 'online';
        $onlineOrderArray = Order::join('order_types', 'order_type_id', '=', 'order_types.id')->where('alias', $orderAlias)->get('client_num');

        //Get active clients
        $activeClients = $this->getActiveClient();
        return view('board', compact('defaultOrderArray', 'onlineOrderArray', 'activeClients'));
    }

    public function getActiveClient()
    {

        $activeOrders = ActiveOrder::orderBy('active_time', 'DESC')->get();

        $array = array();
        foreach ($activeOrders as $activeOrder) {
            $array[] = [
                'client_num' => $activeOrder->client_num,
                'cashbox_num' => $activeOrder->cashbox_num
            ];
        }

        return json_decode(json_encode($array));
    }
}
